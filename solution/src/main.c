#include "transformation.h"
#include "bmp_header.h"
#include "bmp_io.h"
#include "file_io.h"
#include <assert.h>
#include <malloc.h>
int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Please enter input and output file :<");
        return 1;
    }
    FILE* in = NULL;
    FILE* out = NULL;

    if (file_open(&in, argv[1], "rb") == false || file_open(&out, argv[2], "wb") == false)
    {
        fprintf(stderr, "File open error");
        return 1;
    }
    struct image image = { 0 };
    enum bmp_read_status read_status = from_bmp(in, &image);
    if (read_status == READ_INVALID_SIGNATURE)
    {
        fprintf(stderr, "Invalid signature");
        if (!file_close(&in)){
            fprintf(stderr, "Cannot close file!");
        }
        if (!file_close(&out)){
            fprintf(stderr, "Cannot close file!");
        }
    }
    if (read_status == READ_INVALID_HEADER)
    {
        fprintf(stderr, "Invalid header");
        if (!file_close(&in)){
            fprintf(stderr, "Cannot close file!");
        }
        if (!file_close(&out)){
            fprintf(stderr, "Cannot close file!");
        }
        return 1;
    }
    if (read_status == READ_ERROR)
    {
        fprintf(stderr, "Read error");
        if (!file_close(&in)){
            fprintf(stderr, "Cannot close file!");
        }
        if (!file_close(&out)){
            fprintf(stderr, "Cannot close file!");
        }
        return 1;
    }
    struct image rotated_image = rotate_90(&image);

    if (to_bmp(out, &rotated_image) != WRITE_OK)
    {
        fprintf(stderr, "Can't write");
        if (!file_close(&in)){
            fprintf(stderr, "Cannot close file!");
        }
        if (!file_close(&out)){
            fprintf(stderr, "Cannot close file!");
        }
        return 1;
    }
    if (!file_close(&in)){
        fprintf(stderr, "Cannot close file!");
    }
    if (!file_close(&out)){
        fprintf(stderr, "Cannot close file!");
    }
    free(image.pixels);
    free(rotated_image.pixels);
    return 0;
}

