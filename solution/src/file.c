#include "file_io.h"

bool file_open(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    if (*file == NULL) return false;
    return true;

}
bool file_close(FILE** file)
{
    if (fclose (*file) == EOF) return false;
    return true;
}

