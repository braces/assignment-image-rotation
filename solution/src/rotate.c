#include "transformation.h"

#include <malloc.h>
#include <stdio.h>
struct image rotate_90(struct image const* source) {
    struct image result = { 0 };
    result.width = source->height;
    result.height = source->width;
    result.pixels = malloc(sizeof(struct pixel) * result.height * result.width);
    if(!result.pixels)
    {
        fprintf(stderr, "Could not allocate memory!");
        return result;
    }
    else {
        for (size_t i = 0; i < result.height; i++) {
            for (size_t j = 0; j < result.width; j++) {
                result.pixels[i * result.width + j] = (source->pixels[(result.width - 1 - j) * result.height + i]);
            }
        }
    }
    return result;
}
