#include "bmp_header.h"
#include "bmp_io.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42

#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40
#define BI_BIT_COUNT 24
static enum bmp_read_status read_header(FILE* in, struct bmp_header* header) {

    if (fseek(in, 0, SEEK_END))
    {
        return READ_INVALID_HEADER;
    }
    size_t size = ftell(in);
    if (size >= sizeof(struct bmp_header)) {
        rewind(in);
        if (fread(header, sizeof(struct bmp_header), 1, in)) {
            return READ_OK;
        } else
        {
            return READ_INVALID_HEADER;
        }
    }
    else
    {
        return READ_INVALID_HEADER;
    }
}

enum bmp_read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {


    struct pixel* pixels = malloc(img->width* img->height * sizeof(struct pixel));
    if(!pixels) {
        fprintf(stderr, "Could not allocate memory!");
        return READ_ERROR;
    }
    for (size_t i = 0; i < img->height; i++) {
        if (pixels + i * img->width != 0)
        {
            if (fread(pixels + i * img->width, sizeof(struct pixel), img->width, in) != img -> width)
            {
                return READ_ERROR;
            }

            if (fseek(in, padding, SEEK_CUR))
            {
                return READ_ERROR;
            }
        }
    }
    img->pixels = pixels;
    return READ_OK;

}



static uint8_t get_padding(uint32_t width) {
    return width % 4;
}
enum bmp_read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = { 0 };
    if (read_header(in, &header) == READ_INVALID_HEADER) {
        return READ_INVALID_SIGNATURE;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    return read_pixels(img, in, get_padding(header.biWidth));
}



static struct bmp_header generate_header(const struct image* img,uint32_t size) {
    return (struct bmp_header)  {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
}



static enum bmp_write_status write_image(struct image const* img, FILE* out, uint8_t padding) {

    uint64_t zero = 0;
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, out)  != img->width) {
            return WRITE_ERROR;
        }
        if (fwrite(&zero, 1, padding, out) != padding ) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;


}



enum bmp_write_status to_bmp(FILE* out, struct image* img) {
    const uint32_t img_size = (24 * img->width + get_padding(img->width)) * img->height;
    struct bmp_header header = generate_header(img, img_size);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    const uint8_t padding = get_padding(header.biWidth);
    return write_image(img, out, padding);

}

