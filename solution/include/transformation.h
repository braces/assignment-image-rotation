#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include <image.h>

struct image rotate_90(struct image const* source);

#endif
