#ifndef FILE_IO_H
#define FILE_IO_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE** file, const char* name, const char* mode);
bool file_close(FILE** file);
#endif
