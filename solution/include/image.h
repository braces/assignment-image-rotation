#ifndef IMAGE_H
#define IMAGE_H

#pragma once
#include <stdint.h>
#include <malloc.h>

struct pixel
{
    uint8_t b, g, r;
};

struct image
{
    size_t width;
    size_t height;
    struct pixel* pixels;
};
#endif
